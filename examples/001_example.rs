//! # Title about the code
//!
//! Describe about the main code
//! Doc comment (The markdown format can be used )
//!
//! Execute: `rustdoc 001_example.rs`

/*
 * Block comment
 */

/// # The function show_number
// --snip--
///
/// ## Example
///
/// ```rust
/// let number:i32 = 3;
/// show_number(number);
/// ```
///
/// Execute: `rustdoc 001_example.rs`
pub fn show_number(number:i32) {
    println!("Is `number` = {}", number);
}

/// # The function main
// --snip--
///
/// ## About fn main
///
/// Use pub fn to document the scope
///
/// NOTE: fn by default is private
fn main() {
    /* Comment inside the line */
    let number = 2 + /* 6 + */ 1;
    // Line comment
    show_number(number);
}

/*
 Block comment
*/
