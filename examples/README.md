# Examples

## The source code of the first example

File: [000_example.rs](/examples/000_example.rs)

```rust
fn main() {
    println!("Hello World!");
}
```

## Build and Run

Build the source code

> Rust compiler: `rustc`

```bash
$ rustc 000_example.rs
$ ls
000_example  000_example.rs  ...  README.md
```

Run the binary

```bash
$ ./000_example
Hello World!
```

## Generate documentation

Read the [001_example.rs](/examples/001_example.rs)

> Rust documentation: `rustdoc`

```bash
$ rustdoc 001_example.rs
$ ls
000_example.rs  ...  doc  README.md
```

## Debug with gdb

Build the source code using debug mode with the argument -g

```bash
$ rustc -g 000_example.rs
```

Debug the binary

> Rust debug: `rust-gdb`

```bash
$ rust-gdb 000_example
GNU gdb (Ubuntu 9.2-0ubuntu1~20.04.1) 9.2
...
...
Reading symbols from 000_example...
(gdb) l
1	fn main() {
2		println!("Hello World!");
3	}
(gdb) b 2
Breakpoint 1 at 0x7b84: file 000_example.rs, line 2.
(gdb) r
Starting program: /home/{USERNAME}/tutorial-rust/examples/000_example
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/usr/lib/x86_64-linux-gnu/libthread_db.so.1".

Breakpoint 1, _000_example::main () at 000_example.rs:2
2		println!("Hello World!");
(gdb) c
Continuing.
Hello World!
[Inferior 1 (process {PID}) exited normally]
(gdb) q
```

Notes (debug-gdb):
  1. `l` -> list
  2. `b` -> breakpoint
  3. `r` -> run to breakpoint
  4. `c` -> continue
  5. `q` -> quit

## Table with examples

| Source code      | Problem |
|------------------|---------|
| [000_example.rs](/examples/000_example.rs) | Show the text "Hello World!" |
| [001_example.rs](/examples/001_example.rs) | Using the comments in the code |
