#![no_main]

#[no_mangle]
pub extern "C" fn main(argc: isize, argv: *const *const u8) -> isize {
    println!("Args number: {}", argc);
    println!("Argv: {:?}", argv);
    let arg_u8 = unsafe { &**argv };
    println!("arg_u8: {:?}", arg_u8);
    let arg_char = *arg_u8 as char;
    println!("arg_char: {:?}", arg_char);

    let arg_u8 = unsafe { let tmp = &*argv; tmp.offset(1) };
    println!("arg_u8: {:?}", arg_u8);
    let arg_u8 = unsafe { &*arg_u8 };
    println!("arg_u8: {:?}", arg_u8);
    let arg_char = *arg_u8 as char;
    println!("arg_char: {:?}", arg_char);

    let arg_u8 = unsafe { let tmp = &*argv; tmp.offset(2) };
    println!("arg_u8: {:?}", arg_u8);
    let arg_u8 = unsafe { &*arg_u8 };
    println!("arg_u8: {:?}", arg_u8);
    let arg_char = *arg_u8 as char;
    println!("arg_char: {:?}", arg_char);

    let arg_u8 = unsafe { let tmp = &*argv; tmp.offset(7) };
    println!("arg_u8: {:?}", arg_u8);
    let arg_u8 = unsafe { &*arg_u8 };
    println!("arg_u8: {:?}", arg_u8);
    let arg_char = *arg_u8 as char;
    println!("arg_char: {:?}", arg_char);

    let arg_u8 = unsafe { let tmp = &argv; tmp.offset(1) };
    println!("arg_u8: {:?}", arg_u8);
    let arg_u8 = unsafe { let tmp = &*arg_u8; tmp.offset(0) };
    println!("arg_u8: {:?}", arg_u8);
    let arg_u8 = unsafe { &*arg_u8 };
    println!("arg_u8: {:?}", arg_u8);
    let arg_char = *arg_u8 as char;
    println!("arg_char: {:?}", arg_char);

    let arg_u8 = unsafe { let tmp = &argv; tmp.offset(1) };
    println!("arg_u8: {:?}", arg_u8);
    let arg_u8 = unsafe { let tmp = &*arg_u8; tmp.offset(1) };
    println!("arg_u8: {:?}", arg_u8);
    let arg_u8 = unsafe { &*arg_u8 };
    println!("arg_u8: {:?}", arg_u8);
    let arg_char = *arg_u8 as char;
    println!("arg_char: {:?}", arg_char);

    let arg_u8 = unsafe { let tmp = &argv; tmp.offset(1) };
    println!("arg_u8: {:?}", arg_u8);
    let arg_u8 = unsafe { let tmp = &*arg_u8; tmp.offset(5) };
    println!("arg_u8: {:?}", arg_u8);
    let arg_u8 = unsafe { &*arg_u8 };
    println!("arg_u8: {:?}", arg_u8);
    let arg_char = *arg_u8 as char;
    println!("arg_char: {:?}", arg_char);

    0
}
