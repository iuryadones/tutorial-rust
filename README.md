# Tutorial Rust

This tutorial is to use on Linux

## Install Rust and Cargo

```bash
$ curl https://sh.rustup.rs -sSf | sh
```

Install on the ArchLinux

```bash
$ pacman -S rustup
```

## The repository

```
.
├── examples
│   ├── 000_example.rs
│   ├── 001_example.rs
│   └── README.md
├── projects
│   ├── shared_libraries
│   │   ├── lib_c_to_rust
│   │   ├── lib_rust_to_android
│   │   ├── lib_rust_to_java
│   │   ├── lib_rust_to_kotlin
│   │   └── lib_rust_to_python
│   └── webapps
│       └── yew-init-app
├── README.md
└── scripts
    ├── cmain.rs
    └── fibonacci.rs
```

## Reference
  - [cargo-and-rustup-install](https://doc.rust-lang.org/cargo/getting-started/installation.html)
  - [rust-by-example](https://doc.rust-lang.org/stable/rust-by-example/index.html)
  - [programiz-rust-getting-started](https://www.programiz.com/rust/getting-started)
