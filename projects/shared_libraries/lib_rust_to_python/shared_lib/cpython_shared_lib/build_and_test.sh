#!/bin/bash

echo ":>---------------------<:"

echo ":>: Running test"
cargo test
echo ":>: Finished test"

echo ":>---------------------<:"

echo ":>: Running build - DEBUG"
cargo build
echo ":>: Finished build - DEBUG"
echo ":>: Copy ./target/debug/libcalc.so to ./calc.so - DEBUG"
cp ./target/debug/libcalc.so ./calc.so
echo ":>: Running python - import calc - DEBUG"
time python -c 'import calc; print("Calling calc.add(2,3) =", calc.add(2,3))'
echo ":>: Finished python import calc - DEBUG"
echo ":>: Running clean - DEBUG"
cargo clean
echo ":>: Finished clean - DEBUG"

echo ":>---------------------<:"

echo ":>: Running build - RELEASE"
cargo build --release
echo ":>: Finished build - RELEASE"
echo ":>: Copy ./target/release/libcalc.so to ./calc.so - RELEASE"
cp ./target/release/libcalc.so ./calc.so
echo ":>: Running python - import calc - RELEASE"
time python -c 'import calc; print("Calling calc.add(2,3) =", calc.add(2,3))'
echo ":>: Running clean - RELEASE"
cargo clean
echo ":>: Finished clean - RELEASE"

echo ":>---------------------<:"
