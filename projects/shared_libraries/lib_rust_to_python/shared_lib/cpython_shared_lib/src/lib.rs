use cpython::{py_fn, py_module_initializer, PyResult, Python};

pub fn rs_add(left: isize, right: isize) -> isize {
    return left + right;
}

fn py_add(_: Python, a: isize, b: isize) -> PyResult<isize> {
    let out = rs_add(a, b);
    return Ok(out);
}

py_module_initializer!(calc, |py, m| {
    m.add(py, "__doc__", "This module is implemented in Rust.")?;
    m.add(py, "add", py_fn!(py, py_add(value_1: isize, value_2: isize)))?;
    return Ok(());
});

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works_one() {
        let result = rs_add(2, 2);
        assert_eq!(result, 4);
    }

    #[test]
    fn it_works_two() {
        let result = rs_add(-2, 2);
        assert_eq!(result, 0);
    }
}
