use pyo3::prelude::*;
use pyo3::wrap_pyfunction;

pub fn rs_add(left: isize, right: isize) -> isize {
    return left + right;
}

#[pyfunction]
fn add(value_1: isize, value_2: isize) -> PyResult<isize> {
    let result: isize = rs_add(value_1, value_2);
    return Ok(result);
}

#[pymodule]
fn calc(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(add, m)?)?;
    return Ok(());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works_one() {
        let result = rs_add(2, 2);
        assert_eq!(result, 4);
    }

    #[test]
    fn it_works_two() {
        let result = rs_add(-1, 2);
        assert_eq!(result, 1);
    }
}
