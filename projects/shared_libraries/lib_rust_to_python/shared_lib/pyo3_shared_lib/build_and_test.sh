#!/bin/bash

echo ":>---------------------<:"

echo ":>: Running test"
cargo test
echo ":>: Finished test"

echo ":>---------------------<:"

echo ":>: Running build - DEBUG"
cargo build
echo ":>: Finished build - DEBUG"
echo ":>: Copy ./target/debug/libcalc.so to ./python/calc.so - DEBUG"
cp ./target/debug/libcalc.so ./python/calc.so
echo ":>: Running code python - cat ./python/main.py"
cat ./python/main.py
echo ":>: Finished code python - cat ./python/main.py"
echo ":>: Running python - import calc - DEBUG"
python ./python/main.py
echo ":>: Finished python import calc - DEBUG"
echo ":>: Running clean - DEBUG"
cargo clean
echo ":>: Finished clean - DEBUG"

echo ":>---------------------<:"

echo ":>: Running build - RELEASE"
cargo build --release
echo ":>: Finished build - RELEASE"
echo ":>: Copy ./target/release/libcalc.so to ./python/calc.so - RELEASE"
cp ./target/release/libcalc.so ./python/calc.so
echo ":>: Running code python - cat ./python/main.py"
cat ./python/main.py
echo ":>: Finished code python - cat ./python/main.py"
echo ":>: Running python - import calc - RELEASE"
python ./python/main.py
echo ":>: Finished python import calc - RELEASE"
echo ":>: Running clean - RELEASE"
cargo clean
echo ":>: Finished clean - RELEASE"

echo ":>---------------------<:"
