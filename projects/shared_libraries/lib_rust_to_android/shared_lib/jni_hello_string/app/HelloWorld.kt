package app

class HelloWorld {

    init {
        System.loadLibrary("helloworld");
    }

    external fun hello(input: String): String;

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            var output = HelloWorld().hello("iury");
            println("$output");
        }
    }

}
