package app;

class HelloWorld {

    static {
        System.loadLibrary("helloworld");
    }

    private static native String hello(String input);

    public static void main(String[] args) {
        String output = HelloWorld.hello("Iury Adones");
        System.out.println(output);
    }

}
