package com.minimal.hello

class HelloWorld {
    init {
        System.loadLibrary("helloworld")
    }

    external fun hello(input: String): String
}
