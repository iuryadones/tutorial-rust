package com.minimal.hello

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.widget.TextView


class MainActivity : Activity() {
    var TAG: String = "KOTLIN_minimal_HelloWorld"
    var result: String = ""
    var name: String = "Iury Adones"

    val helloWorld = HelloWorld()

    lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.i(TAG, "callback --> onCreate()")
        textView = findViewById(R.id.my_text)
        textView.setText("*** onCreate() ***")
        result = helloWorld.hello(name)
        textView.setText(result)
        Log.i(TAG, "callback onCreate() EXIT OK")
    }

    override fun onStart() {
        super.onStart()
        Log.i(TAG, "callback --> onStart()")
        textView.setText("*** onStart() ***")
        result = helloWorld.hello(name)
        textView.setText(result)
        Log.i(TAG, "callback onStart() EXIT OK")
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "callback -> onResume()")
        textView.setText("*** onResume() ***")
        result = helloWorld.hello(name)
        textView.setText(result)
        Log.i(TAG, "callback -> onResume() EXIT OK")
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "callback -> onPause()")
        textView.setText("*** onPause() ***")
        Log.i(TAG, "callback -> onPause() EXIT OK")
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "callback -> onStop()")
        textView.setText("*** onStop() ***")
        Log.i(TAG, "callback -> onStop() EXIT OK")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "callback -> onDestroy()")
        textView.setText("*** onDestroy() ***")
        Log.i(TAG, "callback -> onDestroy() EXIT OK")
    }

}
