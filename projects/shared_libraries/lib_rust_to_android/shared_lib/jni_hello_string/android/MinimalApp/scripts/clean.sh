#!/bin/bash

printf "Starting cleanup\n"
printf "Remove build tree components\n"

rm -fr ./build/*.apk
rm -fr ./build/*.idsig
rm -fr ./build/apk/classes.dex

find ./build/         -type f -name "R.java"    -exec rm -fr {} \; 2>/dev/null;
find ./build/         -type f -name "*.class"   -exec rm -fr {} \; 2>/dev/null;
find ./build/apk/lib/ -type f -name "*.so"      -exec rm -fr {} \; 2>/dev/null;

printf "done\n"

printf "Remove dir build\n"
rm -fr ./build/
printf "done\n"

printf "Finished cleanup\n"
