#!/bin/bash

mkdir -p ./build/apk/lib/
mkdir -p ./build/gen
mkdir -p ./build/obj

parm=$#

cp -r ./jniLibs/* ./build/apk/lib/

mn="AndroidManifest.xml"

printf "\n* Android Asset Packaging Tool Section *\n"
printf "Now we are going to create R.java file ... "

aapt package -f -m -M $mn -S ./res/ -A ./assets/ \
    -I "${ANDROID_PLATFORM}/android.jar" -J ./build/gen/

rc=$?
if [ $rc -ne 0 ]
    then
        printf "aapt done with ERR = %d\n" $rc
    exit -1
fi

printf "done\n"

rjv=$(find ./build/gen -type f -name "R.java")
ls -l $rjv

if [ $ANDROID_LANG = "java" ]
then
    jf=$(find "./java/com/minimal/" -type d|grep -v "/"$)"/*.java"

    printf "Now we are going to create class files (.class) ... "

    printf "\n* Java Compiler Section - "
    javac -source 1.8 -target 1.8 -bootclasspath "${JAVA_HOME}/jre/lib/rt.jar" \
        -classpath "${ANDROID_PLATFORM}/android.jar" \
        -d build/obj $rjv $jf
fi

if [ $ANDROID_LANG = "kotlin" ]
then
    kf=$(find "./kotlin/com/minimal/" -type d|grep -v "/"$)"/*.kt"

    printf "Now we are going to create class files (.class) ... "

    printf "\n* Kotlin Compiler Section - "
    kotlinc $kf $rjv -jvm-target 1.8 \
        -cp "${ANDROID_PLATFORM}/android.jar" \
        -Xuse-javac -Xcompile-java -Xvalidate-bytecode -Xvalidate-ir \
        -Xabi-stability=stable \
        -d ./build/obj/
fi

rc=$?
if [ $rc -ne 0 ]
    then
    if [ $ANDROID_LANG = "java" ]
    then
        printf "javac done with ERR = %d\n" $rc
        exit -1
    fi
    if [ $ANDROID_LANG = "kotlin" ]
    then
        printf "kotlinc done with ERR = %d\n" $rc
        exit -1
    fi
fi

printf "done\n"

ls -l $(find ./ -type f -name "*.class")

printf "\n* Dalvik byte-code Generation Section *\n"
printf "Now we are going to create the byte code file (dex) ... "

if [ $ANDROID_LANG = "java" ]
then
dx  --dex --output=./build/apk/classes.dex ./build/obj
fi

if [ $ANDROID_LANG = "kotlin" ]
then
d8 ./build/obj/com/minimal/hello/*.class $KOTLIN_HOME/lib/kotlin-stdlib.jar \
    --lib $ANDROID_PLATFORM/android.jar \
    --release --output ./build/apk
fi

printf "done\n"

ls -l $(find ./ -type f -name "*.dex")

uapk="./build/"$(pwd|sed 's/\//\n/g'|tail -n 1)".unsigned.apk"

printf "\n* Android Application Package (APK) Generation Section *\n"
printf "Now we are going to create apk file (.apk) ... "

# Last packing step including assets/ area
aapt package -f -M $mn -S res/ -I "${ANDROID_PLATFORM}/android.jar" -A assets/ -F $uapk build/apk/

rc=$?
if [ $rc -ne 0 ]
    then
        printf "aapt done with ERR = %d\n" $rc
    exit -1
fi

printf "done\n"

ls -l $(find ./ -type f -name "*.apk")

lapk="./build/"$(pwd|sed 's/\//\n/g'|tail -n 1)".aligned.apk"

printf "\n* APK on 4-byte boundaries Generation Section *\n"
printf "Now we are going to create aligned-apk file (.apk) ... "

zipalign -f -p 4 $uapk $lapk

printf "done\n"
ls -l $(find ./ -type f -name "*.apk")

printf "\n* Java Key Store Generaction Section *\n"
printf "Now we are going to create Signing-Key file (.jks) ... "

rm -fr ./build/keystore.jks

keytool -genkeypair -dname "cn=Iury Adones, ou=Assembler, o=Minimal, c=BR" \
        -keystore ./build/keystore.jks -alias androidkey -validity 10000 -keyalg RSA -keysize 2048 \
        -storepass android -keypass android -deststoretype pkcs12

printf "done\n"
ls -l $(find ./ -type f -name "*.jks")

apk="./build/"$(pwd|sed 's/\//\n/g'|tail -n 1)".apk"

printf "\n* Java Key Signing Generaction Section *\n"
printf "Now we are going to create Signing-Key file (.idsig) and final .apk file ... "
apksigner sign --ks ./build/keystore.jks --ks-key-alias androidkey \
               --ks-pass pass:android --key-pass pass:android \
               --out $apk $lapk
printf "done\n"

ls -l $(find ./ -type f -name "*.idsig")
ls -l $apk
