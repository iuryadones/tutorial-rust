# Minimal App

# Struture of the project

Explore all files

```
.
├── AndroidManifest.xml
├── assets
│   └── README.md
├── java
│   └── com
│       └── minimal
│           └── hello
│               ├── HelloWorld.java
│               └── MainActivity.java
├── jniLibs
│   ├── arm64-v8a
│   │   └── libhelloworld.so
│   └── armeabi-v7a
│       └── libhelloworld.so
├── kotlin
│   └── com
│       └── minimal
│           └── hello
│               ├── HelloWorld.kt
│               └── MainActivity.kt
├── README.md
├── res
│   └── layout
│       └── activity_main.xml
└── scripts
    ├── build.sh
    ├── clean.sh
    ├── install.sh
    ├── logcat.sh
    ├── setup.sh
    └── uninstall.sh
```

## Setup and Build

```bash
$ source ./scripts/setup.sh
$ ./scripts/build.sh
```

NOTE: kotlin is default
Change to java the variable `ANDROID_LANG` into ./script/setup.sh

## Install the APK

APK: `build/MinimalApp.apk`

Install in your smartphone this apk file

Config developer mode

```
-> Enable developer mode
-> Enable debugging for wifi
-> Pair device with code
```

Usage adb to pair and connect the device

```bash
$ adb pair ip_address:port
$ adb connect ip_address:port
$ adb devices
```

Install usage the adb install by script

```bash
$ ./scripts/install
```

## Read the Log from APP

```bash
$ ./scripts/logcat.sh
```

## Cleanup and Uninstall

Cleanup the project

```bash
$ ./scripts/clean.sh
```

Uninstall APP

```bash
$ ./scripts/uninstall.sh
```

Disconnect device

```bash
$ adb disconnect ip_address:port
```

## Reference
 1. [Build apk from cli](https://authmane512.medium.com/how-to-build-an-apk-from-command-line-without-ide-7260e1e22676) 
