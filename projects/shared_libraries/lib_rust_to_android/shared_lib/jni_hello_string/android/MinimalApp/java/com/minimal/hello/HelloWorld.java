package com.minimal.hello;

public class HelloWorld {

    static {
        System.loadLibrary("helloworld");
    }

    public static native String hello(String name_string);

}
