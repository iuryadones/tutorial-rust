package com.minimal.hello;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.content.Context;
import android.util.Log;
import android.net.Uri;
import android.content.res.AssetManager;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;

// mylib
import com.minimal.hello.HelloWorld;

public class MainActivity extends Activity {

    String TAG = "JAVA_minimal_HelloWorld";
    String result;
    String name = "Iury Adones";

    HelloWorld hello_world;

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "callback --> onCreate()");
        setContentView(R.layout.activity_main);
        text = (TextView)findViewById(R.id.my_text);
        text.setText("*** onCreate() ***");

        result = hello_world.hello(name);
        text.setText(result);

        Log.i(TAG, "callback onCreate() EXIT OK");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "callback --> onStart()");
        text.setText("*** onStart() ***");
        result = hello_world.hello(name);
        text.setText(result);
        Log.i(TAG, "callback onStart() EXIT OK");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "callback -> onResume()");
        text.setText("*** onResume() ***");
        result = hello_world.hello(name);
        text.setText(result);
        Log.i(TAG, "callback -> onResume() EXIT OK");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "callback -> onPause()");
        text.setText("*** onResume() ***");
        Log.i(TAG, "callback -> onPause() EXIT OK");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "callback -> onStop()");
        text.setText("*** onStop() ***");
        Log.i(TAG, "callback -> onStop() EXIT OK");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "callback -> onDestroy()");
        text.setText("*** onDestroy() ***");
        Log.i(TAG, "callback -> onDestroy() EXIT OK");
    }
}
