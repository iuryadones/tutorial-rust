pub mod core;
use crate::core::rs_hello;

use jni::JNIEnv;
use jni::objects::{JClass, JString};
use jni::sys::jstring;

#[no_mangle]
pub extern "system" fn Java_app_HelloWorld_hello(
    mut env: JNIEnv,
    _class: JClass,
    input: JString
) -> jstring {

    let input: String = env
        .get_string(&input)
        .expect("Couldn't get java string!")
        .into();

    let output = env
        .new_string(rs_hello(input))
        .expect("Coudn't create java string!");

    return output.into_raw();

}

#[no_mangle]
pub extern "system" fn Java_com_minimal_hello_HelloWorld_hello(
    mut env: JNIEnv,
    _class: JClass,
    input: JString
) -> jstring {

    let input: String = env
        .get_string(&input)
        .expect("Couldn't get java string into android!")
        .into();

    let output = env
        .new_string(rs_hello(input))
        .expect("Coudn't create java string into android!");

    return output.into_raw();

}
