/// # Function rs_hello
///
/// Example:
///
/// ```rust
///     let result = rs_hello("Iury".to_string());
///
///     assert_eq!(result, "Hello, Iury!");
/// ```
pub fn rs_hello(input: String) -> String {
    return format!("Hello, {}!", input);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = rs_hello("iury".to_string());
        assert_eq!(result, "Hello, iury!");
    }

}
