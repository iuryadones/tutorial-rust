# Projeto Rust to Android

## Setup

```bash
$ sudo pacman -S jdk8-openjdk
$ sudo pacman -S clang llvm lld
$ sudo paru -S sdkmanager
```

```bash
$ sudo sdkmanager --list
$ sudo sdkmanager "build-tools;30.0.3"
$ sudo sdkmanager "platform-tools;30.0.5"
$ sudo sdkmanager "ndk;24.0.8215888"
$ sudo sdkmanager "platforms;android-30"
```

```bash
$ rustup target add aarch64-linux-android
$ rustup target add armv7-linux-androideabi
```

## Build

Build and Run with Java and Kotlin

```bash
$ make
```

Build to android

```bash
$ cargo build --target aarch64-linux-android
$ cargo build --target aarch64-linux-android --release
$ cargo build --target armv7-linux-androideabi
$ cargo build --target armv7-linux-androideabi --release
```

Copy the release/helloworld.so

```bash
$ cp target/aarch64-linux-android/release/libhelloworld.so \
     android/MinimalApp/jniLibs/arm64-v8a/libhelloworld.so
$ cp target/armv7-linux-androideabi/release/libhelloworld.so \
     android/MinimalApp/jniLibs/armeabi-v7a/libhelloworld.so
```

## Generate APK

Read the android/MinimalApp/README.md
