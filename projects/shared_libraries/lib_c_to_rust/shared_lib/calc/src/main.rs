#[link(name = "calc", kind = "dylib")]
extern "C" {
    fn calc_add(v1:f32, v2:f32) -> f32;
}

fn main() {
    println!("***");
    println!("Load libcalc.so from C with the RUST!");
    println!("***");

    let value_1:f32 = 1.2;
    let value_2:f32 = 2.7;

    let result = unsafe { calc_add(value_1, value_2) };

    println!("--- Shared library ---");
    println!("calc_add({}, {}): {}", value_1, value_2, result);
    println!("---");
}
