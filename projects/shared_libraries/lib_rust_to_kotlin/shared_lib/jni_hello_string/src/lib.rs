use jni::JNIEnv;
use jni::sys::jstring;
use jni::objects::{JClass, JString};

#[no_mangle]
pub extern "system" fn Java_app_HelloWorld_hello(
    env: JNIEnv,
    _class: JClass,
    input: JString
) -> jstring {

    let input: String = env
        .get_string(input)
        .expect("Couldn't get java string!")
        .into();

    let output = env
        .new_string(rs_hello(input))
        .expect("Coudn't create java string!");

    return output.into_raw();

}

pub fn rs_hello(input: String) -> String {
    return format!("Hello, {}!", input);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = rs_hello("iury".to_string());
        assert_eq!(result, "Hello, iury!");
    }
}
