# Rust with Kotlin

```
.
├── app
│   └── HelloWorld.kt
├── Cargo.lock
├── Cargo.toml
├── Makefile
├── README.md
└── src
    └── lib.rs
```

# Run

```bash
$ make
```
