#include <jni.h>

#ifndef _INCLUDE_HELLOWORLD_
#define _INCLUDE_HELLOWORLD_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class: HelloWorld
 * Method: hello
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_app_HelloWorld_hello
    (JNIEnv *, jclass, jstring);

#ifdef __cplusplus
}
#endif

#endif
