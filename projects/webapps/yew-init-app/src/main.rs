use yew::prelude::*;

#[function_component(App)]
fn app() -> Html {
    html! {
    <>
        <h1>{ "Hello World" }</h1>
        <p>{ "WebApp, the yew library was used for frontend with the component as function" }</p>
    </>
    }
}

fn main() {
    yew::start_app::<App>();
}
