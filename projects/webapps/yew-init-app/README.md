# Project Init Webapp

The minimal project of webapp with the framework yew and using functions component

## Setup

```bash
$ rustup target add wasm32-unknown-unknown
$ cargo install trunk --locked
$ cargo install wasm-bindgen-cli
```

## Run serve

Mode: dev

```bash
$ trunk serve --open
```

Mode: release

```bash
$ trunk serve --open --release
```

# Reference
  1. [tutorial-yew](https://yew.rs/docs/tutorial)
